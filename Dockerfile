FROM --platform=${TARGETPLATFORM} gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-openvscode-server:latest

## software needed

RUN                                             \
     apt-get update                             \
  && apt-get install -y --no-install-recommends \
          clang                                 \
          clangd                                \
          cmake                                 \
          gdb                                   \
          g++                                   \
          googletest                            \
          libboost-all-dev                      \
          libgmp-dev                            \
          libtbb-dev                            \
          lldb                                  \
          make                                  \
          valgrind                              \
  && apt-get clean                              \
  && rm -rf /var/lib/apt/lists/*

RUN                                                           \
     update-alternatives                                      \
          --install /usr/bin/cc   cc    /usr/bin/clang    100 \
  && update-alternatives                                      \
          --install /usr/bin/c++  c++   /usr/bin/clang++  100

## Build googletest with installed compiler
RUN                                                                \
     mkdir -p /tmp/gtest                                           \
  && cd /tmp/gtest                                                 \
  && cmake /usr/src/googletest/googletest                          \
  && make all                                                      \
  && mv /tmp/gtest/lib/* /usr/lib/                                 \
  && mv /usr/src/googletest/googletest/include/gtest /usr/include/ \
  && cd /                                                          \
  && rm -r /tmp/gtest

## Library cppcoro needed for coroutine demos
RUN                                                       \
     cd /tmp                                              \
  && git clone https://github.com/andreasbuhr/cppcoro.git \
  && cd cppcoro                                           \
  && export PATH=/home/linuxbrew/.linuxbrew/bin:$PATH     \
  && cmake .                                              \
  && make                                                 \
  && make install                                         \
  && rm -r cppcoro

# C++ extension

RUN                                                                   \
     mkdir -p /init-config/.openvscode-server/extensions              \
  && /app/openvscode-server/bin/openvscode-server                     \
          --extensions-dir /init-config/.openvscode-server/extensions \
          --install-extension vadimcn.vscode-lldb                     \
  && /app/openvscode-server/bin/openvscode-server                     \
          --extensions-dir /init-config/.openvscode-server/extensions \
          --install-extension llvm-vs-code-extensions.vscode-clangd

RUN                                          \
     mkdir -p /init-config/.config/clangd    \
  && mkdir -p /init-config/workspace/.vscode \
  && mkdir -p /init-config/.openvscode-server/data/Machine

COPY \
  config.yaml /init-config/.config/clangd/
COPY \
   tasks.json /init-config/workspace/.vscode/
COPY \
  launch.json /init-config/workspace/.vscode/
COPY \
  settings.json /init-config/.openvscode-server/data/Machine/

RUN \
     echo "set disable-randomization off" >/init-config/.gdbinit
