# docker-openvscode-server-cpp

## What

This image is based on [docker-openvscode-server](https://gitlab-research.centralesupelec.fr/my-docker-images/docker-openvscode-server),
it adds the needed tools for C/C++ development.

Included tools are:
- g++-13 (as g++), gcc-13 (as gcc)
- clang++-18 (as c++), clang-18 (as cc)
- clangd with its [VSCode extension](https://marketplace.visualstudio.com/items?itemName=llvm-vs-code-extensions.vscode-clangd)
- gdb
- lldb
- VSCode [extension for lldb](https://github.com/vadimcn/vscode-lldb)
- [CMake](https://cmake.org)
- [Boost](www.boost.org)
- [GMP](https://gmplib.org)
- [OneTBB](https://spec.oneapi.io/versions/latest/elements/oneTBB/source/nested-index.html)
- [Valgrind](https://valgrind.org)
- [GoogleTest](https://github.com/google/googletest)
- [cppcoro](https://github.com/andreasbuhr/cppcoro)


## Details

- The exposed port is 3000
- The user folder is `/config`
- the user and sudo password is `abc`
- If docker (or podman) is installed on your computer, you can run (`amd64` or `arm64` architecture) this 
  image, assuming you are in a specific folder that will be shared with the container at 
  `/config`, with:
  
  `docker run -p 3000:3000 -v "$(pwd):/config"
    gitlab-research.centralesupelec.fr:4567/my-docker-images/docker-openvscode-server-cpp:latest`

